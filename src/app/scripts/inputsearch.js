let page = 1;
var callInput = () =>{
const url = `https://www.cheapshark.com/api/1.0/deals?storeID=1&pageSize=60`
page += 1;
steamFetch(url);
loader.classList.toggle('hide');
}

var steamFetch = (url) =>{
    fetch(url)
    .then((response) => response.json())
    .then((myJson) => {
        const steamOffers = myJson;
        inputSearch(steamOffers)
    })
    .catch((error) => console.log(error))
};

const steamUrl = 'https://store.steampowered.com/app/';

var inputSearch =(paramOne)=>{

    const mainContainer = document.getElementById('container');
    const topDeals = document.getElementById('top-deals');

    if(!document.getElementById('input-results')){
        const results = document.createElement('div');
        results.setAttribute('id', 'input-results');
        results.setAttribute('class', 'row bg-light');
        mainContainer.insertBefore(results, topDeals);
    }else{};

    let inputResults = paramOne.map(index => ({ID: index.gameID, Title: index.title, NormalPrice: index.normalPrice, SalePrice: index.salePrice, SteamURL: index.steamAppID, Image: index.thumb, ReviewPercent: index.steamRatingPercent}));
    var htmlInput = document.getElementById('search-input');
    let inputValue = htmlInput.value;

    inputResults.forEach(element => {
        let titles = element.Title;
        if(titles.includes(inputValue) && inputValue.length >4){
            const results = document.getElementById('input-results');
            results.innerHTML = '';
            const divContainerInputTitleResults = document.createElement('div');
            divContainerInputTitleResults.classList.add('print-container', 'mx-auto');
            results.appendChild(divContainerInputTitleResults);

            const offerPrice = document.createElement('div');
            offerPrice.classList.add('offer-price');
            divContainerInputTitleResults.appendChild(offerPrice);

            const list = document.createElement('div');
            divContainerInputTitleResults.appendChild(list);

            const img = document.createElement('img');
            img.classList.add('print-offers__img');
            divContainerInputTitleResults.appendChild(img);

            const buttonOffer = document.createElement('div');
            divContainerInputTitleResults.appendChild(buttonOffer);

            img.src = element.Image;
            offerPrice.textContent = `❌ Antes: ${element.NormalPrice}€  ✔️ Ahora: ${element.SalePrice}€`;
            let dinamicUrl = steamUrl + element.SteamURL;
            buttonOffer.innerHTML += `<a class ="offer-link"href="${dinamicUrl}" target="__blank">Ver oferta</a>`;

            const versusContain = document.getElementById('versus-offers');
            ((versusContain)) ? versusContain.remove() : '';

            const showMoreOffers = document.getElementById('btn-showmore')
            showMoreOffers.classList.toggle('hide');

            const steamOfferContainer = document.getElementById('steam-offers');
            ((steamOfferContainer)) ? steamOfferContainer.remove() : '';

            const showMoreContainer = document.getElementById('show-more-offers');
            const footer = document.getElementById('footer');
            mainContainer.insertBefore(showMoreContainer, footer);
        }else{};
    });
    loader.classList.toggle('hide');
}
export{callInput};