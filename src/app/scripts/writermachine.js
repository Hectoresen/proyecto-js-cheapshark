var text = document.getElementById("writer-machine");
var str = text.innerHTML;

text.innerHTML = "";

var speed = 150;
var i = 0;

function typeWriter() {
    if(i < str.length){
        text.innerHTML += str.charAt(i);
        /* console.log(str.charAt(i)); */
        //https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/String/charAt
        i++;
        setTimeout(typeWriter, speed);
    }
}
setTimeout(typeWriter, speed);

export {typeWriter};


/*
var cualquierCadena="Brave new world";

console.log("El carácter en el índice 0 es '" + cualquierCadena.charAt(0) + "'")
console.log("El carácter en el índice 1 es '" + cualquierCadena.charAt(1) + "'")
console.log("El carácter en el índice 2 es '" + cualquierCadena.charAt(2) + "'")
console.log("El carácter en el índice 3 es '" + cualquierCadena.charAt(3) + "'") */
