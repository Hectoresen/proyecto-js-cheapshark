let page = 1;
let pageUserCount = 0;
const steamUrl = 'https://store.steampowered.com/app/';
var loader = document.getElementById('loader');

var urlSteam = () =>{
    loader.classList.toggle('hide');
    const url = `https://www.cheapshark.com/api/1.0/deals?storeID=1&pageNumber=${page}&pageSize=12`
    page += 1;
    steamFetch(url);
}

var steamFetch = (url) =>{
    fetch(url)
    .then((response) => response.json())
    .then((myJson) => {
        const steamOffers = myJson;
        steamOffer(steamOffers)
    })
    .catch((error) => console.log(error))
};

var steamOffer = (paramOne) =>{
    pageUserCount ++;
    const divMobile = document.getElementById('div-mobile');
    const versusContainer = document.getElementById('versus-offers');
    const mainContainer = document.getElementById('container');

    if(!document.getElementById('steam-offers')){
        const containerOffers = document.createElement('div');
        containerOffers.setAttribute('id', 'steam-offers');
        containerOffers.setAttribute('class', 'row bg-light');
        mainContainer.insertBefore(containerOffers, versusContainer);
    }else{};

    ((versusContainer)) ? versusContainer.remove() : '';

    const showMoreContainer = document.getElementById('show-more-offers');
    const containerOffers = document.getElementById('steam-offers');

    const headerDeals = document.createElement('div');
    containerOffers.appendChild(headerDeals);
    headerDeals.classList.add('header-deals-steam');
    headerDeals.innerHTML = `<p class="pag">Página ${pageUserCount}</p>`;

    paramOne.forEach(function callback(element,index){
        let urlBaseOffer = `https://store.steampowered.com/app/`;
        let urlId = `${element.steamAppId}`

        const divTotalContainer = document.createElement('div');
        divTotalContainer.classList.add('print-container');
        containerOffers.appendChild(divTotalContainer);

        const offerPrice = document.createElement('div');
        offerPrice.classList.add('offer-price');
        divTotalContainer.appendChild(offerPrice);
        offerPrice.textContent = `❌ Antes: ${element.normalPrice}€  ✔️ Ahora: ${element.salePrice}€`;

        const div = document.createElement('div');
        div.classList.add('print-offers');
        divTotalContainer.appendChild(div);

        let dinamicUrl = steamUrl + element.steamAppID;

        const buttonOffer = document.createElement('div');
        divTotalContainer.appendChild(buttonOffer);
        buttonOffer.innerHTML += `<a class ="offer-link"href="${dinamicUrl}" target="__blank">Ir a oferta</a>`

        //90% DTO = PNG
        if(element.normalPrice/element.salePrice >=9){
            const ninetyOff = document.createElement('img');
            ninetyOff.classList.add('ninety-discount');
            div.appendChild(ninetyOff);
            ninetyOff.src = "../assets/90off.png";
        }else{};
        //80% RATING = TOP
        if(element.steamRatingPercent >= 80){
            const topRated = document.createElement('img');
            topRated.classList.add('top-rated');
            divTotalContainer.appendChild(topRated);
            topRated.src = "../assets/toprated.png"
        }else{};

        const img = document.createElement('img');
        img.classList.add('print-offers__img');
        div.appendChild(img);
        img.src = element.thumb;
    });
    loader.classList.toggle('hide');
    var btnShow = document.getElementById('btn-showmore');
    var show = btnShow.classList.toggle('hide');
    ((show)) ? show = btnShow.classList.toggle('hide') : '';

    var results = document.getElementById('input-results');
    ((results)) ? results.remove() : '';

    var topDealsContain = document.getElementById('top-deals');
    ((topDealsContain)) ? topDealsContain.remove() : '';
}

export{urlSteam};
