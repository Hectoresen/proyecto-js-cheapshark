//VERSUS
const steamUrl = 'https://www.cheapshark.com/api/1.0/deals?storeID=1';
const fanaticalUrl = 'https://www.cheapshark.com/api/1.0/deals?storeID=15';
const steamLink = 'https://store.steampowered.com/app/';
var loader = document.getElementById('loader');

const versusData = ()=>{
    loader.classList.toggle('hide');
    var topDealsContain = document.getElementById('top-deals');
    ((topDealsContain)) ? topDealsContain.remove() : '';

    let dataSteam = fetch(steamUrl);
    let dataFanatical = fetch(fanaticalUrl);

    Promise.all([dataSteam,dataFanatical]).then(data =>{
        return Promise.all(data.map(result => result.json()));
    }).then(([steam, fanatical]) =>{
        dataProcess(steam,fanatical);
    })
    .catch()
}

var dataProcess = (steamOffers, fanaticalOffers) =>{
    var freeOfferSteam = steamOffers.filter(item => item.salePrice < 1);
    var freeOfferFanatical = fanaticalOffers.filter(item => item.salePrice < 1);

    var totalOfferSteam = freeOfferSteam.map(index =>({ Shop: 'Steam', Title: index.title, Price: index.salePrice, Image: index.thumb, URL: index.steamAppID, ReviewPercent: index.steamRatingPercent}));
    var totalOfferFanatical = freeOfferFanatical.map(index =>({ Shop: 'Fanatical', Title: index.title, Price: index.salePrice, Image: index.thumb, URL: index.steamAppID, ReviewPercent: index.steamRatingPercent}));

    printOffers(totalOfferSteam,totalOfferFanatical);
    console.log(totalOfferSteam);
    console.log(totalOfferFanatical);

    //Spread operator
    // let total = [...totalOfferSteam, totalOfferFanatical];
    // console.log(total);

/*     class Versus{
        constructor(title, salePrice, thumb, url){
            this._title = title;
            this._salePrice = salePrice;
            this._thumb = thumb;
            this._url = url;
        }
    }
 */
    // ((totalOfferSteam.title == totalOfferFanatical.title)) ? printOffers(totalOfferSteam, totalOfferFanatical) : '';
        // var newOfferSteam = new Versus('hola', totalOfferSteam.salePrice, totalOfferSteam.thumb, totalOfferSteam.url);
}

var printOffers = (finalSteam, finalFanatical) =>{

    var steamContent = document.getElementById('steam-offers');
    var btnShow = document.getElementById('btn-showmore')
    btnShow.classList.contains('hide');

    const inputContain = document.getElementById('input-results');
    ((inputContain)) ? inputContain.remove() : '';
    ((steamContent)) ? steamContent.remove() : '';
    ((btnShow)) ? btnShow.classList.toggle('hide') : '';

    const mainContainer = document.getElementById('container');
    const showMoreOffersContainer = document.getElementById('show-more-offers');

    if(!document.getElementById('versus-offers')){
        const containerOffers = document.createElement('versus-offers');
        containerOffers.setAttribute('id', 'versus-offers');
        containerOffers.setAttribute('class', 'row bg-light');
        mainContainer.insertBefore(containerOffers,showMoreOffersContainer)
    }else{}

    const containerOffers = document.getElementById('versus-offers');

    const divColums = document.createElement('div');
    divColums.classList.add('versus');
    containerOffers.appendChild(divColums);

    const divSteam = document.createElement('div');
    divSteam.classList.add('versus__steam')
    divColums.appendChild(divSteam);

    const headerVersusSteam = document.createElement('div');
    headerVersusSteam.classList.add('versus-header__steam');
    headerVersusSteam.innerHTML = '<a href="#"><span class="fab fa-steam">   STEAM</span></a>'
    divSteam.prepend(headerVersusSteam);

    finalSteam.forEach(function callback(element,index){
        const printOfferSteam = document.createElement('div');
        printOfferSteam.classList.add('print-container__versus');
        divSteam.appendChild(printOfferSteam);

        const offerPrice = document.createElement('div');
        offerPrice.classList.add('versus__text');
        printOfferSteam.appendChild(offerPrice);
        offerPrice.textContent = `🎮${element.Title} ➡️ 🏷️ ${element.Price}€`;

        const steamImg = document.createElement('img');
        steamImg.classList.add('versus__img');
        printOfferSteam.appendChild(steamImg);
        steamImg.src = element.Image;

        if(element.ReviewPercent >= 80){
            const topRated = document.createElement('img');
            topRated.classList.add('versus__top-rated');
            printOfferSteam.appendChild(topRated);
            topRated.src = "../assets/toprated.png"
        }else{};

        let dinamicUrl = steamLink + element.URL;

        const buttonOffer = document.createElement('div');
        printOfferSteam.appendChild(buttonOffer);
        buttonOffer.innerHTML += `<a class ="offer-link"href="${dinamicUrl}" target="__blank">Ir a oferta</a>`
    });

    const divFanatical = document.createElement('div');
    divFanatical.classList.add('versus__fanatical')
    divColums.appendChild(divFanatical);

    const headerVersusFanatical = document.createElement('div');
    headerVersusFanatical.classList.add('versus-header__fanatical');
    headerVersusFanatical.innerHTML = `<a href="#"><span class="fab fa-fantasy-flight-games">  FANATICAL</span></a>`
    divFanatical.appendChild(headerVersusFanatical);

    finalFanatical.forEach(function callback(element,index){
        const printOfferFanatical = document.createElement('div');
        printOfferFanatical.classList.add('print-container__versus');
        divFanatical.appendChild(printOfferFanatical);

        const offerPrice = document.createElement('div');
        offerPrice.classList.add('versus__text');
        printOfferFanatical.appendChild(offerPrice);
        offerPrice.textContent = `🎮${element.Title} ➡️ 🏷️ ${element.Price}€`;

        const fanaticalImg = document.createElement('img');
        fanaticalImg.classList.add('versus__img');
        printOfferFanatical.appendChild(fanaticalImg);
        fanaticalImg.src = element.Image;

        if(element.ReviewPercent >= 80){
            const topRated = document.createElement('img');
            topRated.classList.add('versus__top-rated');
            printOfferFanatical.appendChild(topRated);
            topRated.src = "../assets/toprated.png"
        }else{};

        const buttonOffer = document.createElement('div');
        printOfferFanatical.appendChild(buttonOffer);
        buttonOffer.innerHTML += `<a class ="offer-link"href="#" target="__blank">Ir a oferta</a>`
    });

    if(finalSteam.length > finalFanatical.length){
        const winnerDiv = document.createElement('div');
        winnerDiv.setAttribute('class', 'row bg-light offer-link winner');
        containerOffers.appendChild(winnerDiv);
        winnerDiv.innerHTML = `<a class="offer-link" href="https://store.steampowered.com/?l=spanish" target="__blank">STEAM es el ganador de la comparación</a>`
    }else if(finalFanatical > finalSteam.length){
        const winnerDiv = document.createElement('div');
        winnerDiv.setAttribute('class', 'row bg-light offer-link winner');
        containerOffers.appendChild(winnerDiv);
        winnerDiv.innerHTML = `<a class="offer-link" href="https://www.fanatical.com/en/" target="__blank">FANATICAL es el ganador de la comparación</a>`
    }
    loader.classList.toggle('hide');
}
export{versusData};