let page = 1;
const topDealsUrl = '';
const steamUrl = 'https://store.steampowered.com/app/';

var urlTopDeals = () =>{
const url = `https://www.cheapshark.com/api/1.0/deals?`
page += 1;
topDealsFetch(url);
}

var topDealsFetch = (url) =>{
    fetch(url)
    .then((response) => response.json())
    .then((myJson) => {
        const topDeals = myJson;
        showTopDeals(topDeals)
    })
    .catch((error) => console.log(error))
};

const showTopDeals = (paramOne) =>{

    const containerOffers = document.getElementById('top-deals');
    const headerDeals = document.createElement('div');
    containerOffers.appendChild(headerDeals);
    headerDeals.classList.add('header-deals');

    const imgBestValue = document.createElement('img');
    imgBestValue.classList.add('best-value');
    headerDeals.appendChild(imgBestValue);
    imgBestValue.src="../assets/best-value.png"

    //savings over 95%
    paramOne.forEach(element => {

        if(element.savings >=95){
            const divTotalContainer = document.createElement('div');
            divTotalContainer.classList.add('print-container');
            containerOffers.appendChild(divTotalContainer);

            const offerPrice = document.createElement('div');
            offerPrice.classList.add('offer-price');
            divTotalContainer.appendChild(offerPrice);
            offerPrice.textContent = `❌ Antes: ${element.normalPrice}€  ✔️ Ahora: ${element.salePrice}€`;

            const div = document.createElement('div');
            div.classList.add('print-offers');
            divTotalContainer.appendChild(div);

            let dinamicUrl = steamUrl + element.steamAppID;

            const buttonOffer = document.createElement('div');
            divTotalContainer.appendChild(buttonOffer);
            buttonOffer.innerHTML += `<a class ="offer-link"href="${dinamicUrl}" target="__blank">Ir a oferta</a>`

            //90% DTO = PNG
            if(element.normalPrice/element.salePrice >=9){
                const ninetyOff = document.createElement('img');
                ninetyOff.classList.add('ninety-discount');
                div.appendChild(ninetyOff);
                ninetyOff.src = "../assets/90off.png";
            }else{};
            //80% RATING = TOP
            if(element.steamRatingPercent >= 80){
                const topRated = document.createElement('img');
                topRated.classList.add('top-rated');
                divTotalContainer.appendChild(topRated);
                topRated.src = "../assets/toprated.png"
            }else{};

            const img = document.createElement('img');
            img.classList.add('print-offers__imgTop');
            div.appendChild(img);
            img.src = element.thumb;
        }else{};
    });
}


export{urlTopDeals};

