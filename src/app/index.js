import './styles/styles.scss';
import 'bootstrap';
import {typeWriter} from './scripts/writermachine';
import {urlSteam} from './scripts/steam';
import {callInput} from './scripts/inputsearch';
/* import {urlFanatical} from './scripts/fanatical'; */
import {urlTopDeals} from './scripts/topDeals';
import {versusData} from './scripts/versus';

window.onload = function(){
  addListeners();
  urlTopDeals();
}

var addListeners = function(){
  document.getElementById('steam-offer').addEventListener('click', urlSteam);
  document.getElementById('fanatical-offer').addEventListener('click', versusData);
  document.getElementById('search-input').addEventListener('input', callInput);
  document.getElementById('btn-showmore').addEventListener('click',urlSteam);
  document.getElementById("top-arrow").addEventListener('click', clickScrollTop)
}

var clickScrollTop = () =>{
  rootElement.scrollTo({
    top:0,
    behavior: "smooth"
  })
}

