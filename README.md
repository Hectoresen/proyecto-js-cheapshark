# Proyecto-js-CheapShark

# CheapShark es un sitio web de comparación de precios para juegos de PC digitales. Realizamos un seguimiento de los precios en varias tiendas, 
  incluidas Steam, GreenManGaming, Fanatical y muchas otras.


# Lenguajes utilizados:
	- JS
	- SCSS
	- HTML5
	- Bootstrap

# Metodologías utilizadas:
	- BEM (Parcialmente)


### PROYECTO

# Por defecto: 
	 Existe una sección de TOP VENTAS, que  aparecerá en cuanto iniciemos la web. Esta sección hace una llamada a la api, luego se filtra para conseguir 
 	 solo aquellos juegos que superen un 95% de descuento y aquellos juegos con más de un 80% de reseñas positivas y así mostrarlas en pantalla debajo del buscador.

# Icono STEAM: 
	Creará un listado de todos los juegos de Steam, se ha limitado la página a 12 juegos y se ha creado una variable para cada vez que al pulsar
  	ver más ofertas genere una nueva página con nuevos elementos. También filtrará por aquellos juegos que tengan un descuento superior al 90% y los juegos que tengan 
  	más de un 80% de reseñas positivas, añadiendo una imágen distintiva a cada juego mostrado para guiar al usuario. En este caso, cumplan o no cumplan las condiciones
  	anteriores, el resto de juegos se mostrarán igual. Para los enlaces se ha creado una URL dinámica que lee el SteamAppID de cada elemento y la añade a la URL.
  	Este botón borrará el contenido que se haya cargado con anterioridad si existiese.

# Icono ver más ofertas de Steam: 
	Este botón irá desplegando una nueva página de ofertas de Steam y en la cabecera de cada separación de 12 juegos mostrará el número de página
  	en la que se encuentra el usuario. Este botón borrará el contenido que se haya cargado con anterioridad si existiese.

# Icono VERSUS: 
	Desplegará una lista de juegos separados por columnas con el nombre de cada plataforma a modo de comparación para saber que plataforma tiene más juegos por
  	menos de 1€. La lista se desplegará con los estilos principales de la página, Precio, imágen, filtros de rating y botón con enlace. Además según la plataforma que contenga
  	más juegos por menos de 1€ se desplegará al final de la lista un DIV con un enlace a la plataforma ganadora de la comparación.

# Input: 
	buscará los juegos que el usuario indique, con imágen, precio y enlace.

# Botón inferior arrow top: 
	Te ayudará a volver a la cabecera de la Web cuando te encuentres abajo de todo.


## Extras:

#Partículas de fondo:
https://vincentgarreau.com/particles.js/

Se modifican en la web, se descargan al proyecto y se les coloca un z-index: -1; 
Si quisiésemos interactuar con las partículas deberíamos tener por ejemplo un z-index: 1, pero en el resto de elementos de la web que sean interactivos/clickables debemos
poner un z-index superior al de las partículas.

#Máquina de escribir:


